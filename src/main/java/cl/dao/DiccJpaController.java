/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.dao;

import cl.dao.exceptions.NonexistentEntityException;
import cl.dao.exceptions.PreexistingEntityException;
import cl.entities.Dicc;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author estay
 */
public class DiccJpaController implements Serializable {

    EntityManagerFactory emf = Persistence.createEntityManagerFactory("my_persistence_unit");
 

    public DiccJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }

    public DiccJpaController() {

    }

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Dicc dicc) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(dicc);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findDicc(dicc.getWordId()) != null) {
                throw new PreexistingEntityException("Dicc " + dicc + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Dicc dicc) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            dicc = em.merge(dicc);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = dicc.getWordId();
                if (findDicc(id) == null) {
                    throw new NonexistentEntityException("The dicc with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Dicc dicc;
            try {
                dicc = em.getReference(Dicc.class, id);
                dicc.getWordId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The dicc with id " + id + " no longer exists.", enfe);
            }
            em.remove(dicc);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Dicc> findDiccEntities() {
        return findDiccEntities(true, -1, -1);
    }

    public List<Dicc> findDiccEntities(int maxResults, int firstResult) {
        return findDiccEntities(false, maxResults, firstResult);
    }

    private List<Dicc> findDiccEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Dicc.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Dicc findDicc(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Dicc.class, id);
        } finally {
            em.close();
        }
    }

    public int getDiccCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Dicc> rt = cq.from(Dicc.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
