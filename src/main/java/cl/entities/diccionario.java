
package cl.entities;

import java.io.Serializable;


public class diccionario  implements Serializable{
private String word_id;
private String lenguaje;
private String significado;
   
    public String getWord_id() {
        return word_id;
    }

    
    public void setWord_id(String word_id) {
        this.word_id = word_id;
    }

   
    
    public String getLenguaje() {
        return lenguaje;
    }

  
    
    public void setLenguaje(String lenguaje) {
        this.lenguaje = lenguaje;
    }

    
    
    public String getSignificado() {
        return significado;
    }

    
    
    public void setSignificado(String significado) {
        this.significado = significado;
    }

}
