/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author estay
 */
@Entity
@Table(name = "dicc")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Dicc.findAll", query = "SELECT d FROM Dicc d"),
    @NamedQuery(name = "Dicc.findByWordId", query = "SELECT d FROM Dicc d WHERE d.wordId = :wordId"),
    @NamedQuery(name = "Dicc.findByLenguaje", query = "SELECT d FROM Dicc d WHERE d.lenguaje = :lenguaje"),
    @NamedQuery(name = "Dicc.findBySignificado", query = "SELECT d FROM Dicc d WHERE d.significado = :significado")})
public class Dicc implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "word_id")
    private String wordId;
    @Size(max = 2147483647)
    @Column(name = "lenguaje")
    private String lenguaje;
    @Size(max = 2147483647)
    @Column(name = "significado")
    private String significado;

    public Dicc() {
    }

    public Dicc(String wordId) {
        this.wordId = wordId;
    }

    public String getWordId() {
        return wordId;
    }

    public void setWordId(String wordId) {
        this.wordId = wordId;
    }

    public String getLenguaje() {
        return lenguaje;
    }

    public void setLenguaje(String lenguaje) {
        this.lenguaje = lenguaje;
    }

    public String getSignificado() {
        return significado;
    }

    public void setSignificado(String significado) {
        this.significado = significado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (wordId != null ? wordId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Dicc)) {
            return false;
        }
        Dicc other = (Dicc) object;
        if ((this.wordId == null && other.wordId != null) || (this.wordId != null && !this.wordId.equals(other.wordId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cl.entities.Dicc[ wordId=" + wordId + " ]";
    }
    
}
