package root.services;

import cl.dao.DiccJpaController;
import cl.entities.Dicc;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/diccionario")
public class diccRest {

    EntityManagerFactory emf = Persistence.createEntityManagerFactory("my_persistence_unit");
    EntityManager em;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarTodo() {
       
           
        DiccJpaController dao= new DiccJpaController();
        List<Dicc> lista = dao.findDiccEntities();
        System.out.println("lista.size():" + lista.size());
        return Response.ok(200).entity(lista).build();
    }

    @GET
    @Path("/{idbuscar}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response buscarId(@PathParam("idbuscar") String idbuscar) {
        DiccJpaController dao = new DiccJpaController();
        Dicc sel = dao.findDicc(idbuscar);
        return Response.ok(200).entity(sel).build();
    }

    
    }

